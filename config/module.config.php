<?php

namespace Lerp\Office;

use Lerp\Office\Factory\Service\Equipment\TimesheetSpreadsheetServiceFactory;
use Lerp\Office\Factory\Service\Order\OrderAnalysisGroupedSpreadsheetServiceFactory;
use Lerp\Office\Factory\Service\Order\OrderMasterDataServiceFactory;
use Lerp\Timesheet\Service\Equipment\TimesheetSpreadsheetServiceInterface;
use Lerp\Order\Service\OrderAnalysisGroupedSpreadsheetServiceInterface;
use Lerp\Order\Service\OrderMasterDataServiceInterface;

return [
    'service_manager' => [
        'factories'  => [
            // service - order
            OrderMasterDataServiceInterface::class                 => OrderMasterDataServiceFactory::class,
            OrderAnalysisGroupedSpreadsheetServiceInterface::class => OrderAnalysisGroupedSpreadsheetServiceFactory::class,
            // service - equipment
            TimesheetSpreadsheetServiceInterface::class            => TimesheetSpreadsheetServiceFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helpers'    => [
        'factories'  => [],
        'invokables' => [],
        'aliases'    => [],
    ],
    'lerp_office'     => [],
];
