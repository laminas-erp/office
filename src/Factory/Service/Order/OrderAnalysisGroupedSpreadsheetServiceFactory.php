<?php

namespace Lerp\Office\Factory\Service\Order;

use Lerp\Office\Service\Order\OrderAnalysisGroupedSpreadsheetService;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Order\Table\Order\OrderAnalysisTable;

class OrderAnalysisGroupedSpreadsheetServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OrderAnalysisGroupedSpreadsheetService();
        $service->setLogger($container->get('logger'));
        $service->setTmpFolder(realpath($container->get('config')['lerp_application']['tmp_dir']));
        $service->setNumberFormatService($container->get(NumberFormatService::class));
        $service->setOrderAnalysisTable($container->get(OrderAnalysisTable::class));
        return $service;
    }
}
