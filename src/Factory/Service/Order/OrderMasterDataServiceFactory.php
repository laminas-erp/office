<?php

namespace Lerp\Office\Factory\Service\Order;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Document\Service\OfferDocumentService;
use Lerp\Document\Service\OrderDocumentService;
use Lerp\Factoryorder\Service\FactoryorderService;
use Lerp\Order\Service\Order\Files\FileOrderRelService;
use Lerp\Order\Service\Order\OrderItemService;
use Lerp\Order\Service\Order\OrderService;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderService;
use Lerp\Office\Service\Order\OrderMasterDataService;
use Lerp\Stock\Service\StockService;

class OrderMasterDataServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new OrderMasterDataService();
        $service->setLogger($container->get('logger'));
        $service->setTmpFolder(realpath($container->get('config')['lerp_application']['tmp_dir']));
        $service->setOrderService($container->get(OrderService::class));
        $service->setOrderItemService($container->get(OrderItemService::class));
        $service->setFileOrderRelService($container->get(FileOrderRelService::class));
        $service->setOfferDocumentService($container->get(OfferDocumentService::class));
        $service->setOrderDocumentService($container->get(OrderDocumentService::class));
        $service->setPurchaseOrderService($container->get(PurchaseOrderService::class));
        $service->setFactoryorderService($container->get(FactoryorderService::class));
        $service->setStockService($container->get(StockService::class));
        return $service;
    }
}
