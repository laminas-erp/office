<?php

namespace Lerp\Office\Factory\Service\Equipment;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Timesheet\Table\Equipment\ViewTimesheetTable;
use Lerp\Office\Service\Equipment\TimesheetSpreadsheetService;

class TimesheetSpreadsheetServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new TimesheetSpreadsheetService();
        $service->setLogger($container->get('logger'));
        $service->setTmpFolder(realpath($container->get('config')['lerp_application']['tmp_dir']));
        $service->setViewTimesheetTable($container->get(ViewTimesheetTable::class));
        return $service;
    }
}
