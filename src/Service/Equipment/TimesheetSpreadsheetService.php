<?php

namespace Lerp\Office\Service\Equipment;

use Bitkorn\Trinket\Tools\Render\RenderTool;
use Lerp\Timesheet\Entity\Equipment\ViewTimesheetEntity;
use Lerp\Timesheet\Entity\ParamsTimesheetReport;
use Lerp\Timesheet\Service\Equipment\TimesheetSpreadsheetServiceInterface;
use Lerp\Timesheet\Table\Equipment\ViewTimesheetTable;
use Lerp\Office\Service\AbstractSpreadsheetService;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Writer\Exception as SpreadsheetWriterException;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class TimesheetSpreadsheetService extends AbstractSpreadsheetService implements TimesheetSpreadsheetServiceInterface
{
    protected ViewTimesheetTable $viewTimesheetTable;

    public function setViewTimesheetTable(ViewTimesheetTable $viewTimesheetTable): void
    {
        $this->viewTimesheetTable = $viewTimesheetTable;
    }

    /**
     * @param string $equipmentUuid
     * @param string $timeStart
     * @param string $timeEnd
     * @throws Exception
     */
    public function createTimesheetSpreadsheet(string $equipmentUuid, string $timeStart, string $timeEnd): void
    {
        $params = new ParamsTimesheetReport();
        $params->setOrderField('timesheet_time_start');
        $params->setOrderDirec('ASC');
        $entity = new ViewTimesheetEntity();
        if (
            empty($tss = $this->viewTimesheetTable->equipmentViewTimesheetPeriod($params, $equipmentUuid, $timeStart, $timeEnd))
            || !$entity->exchangeArrayFromDatabase($tss[0])
        ) {
            return;
        }
        $timefStart = !empty($timeStart) ? date_create($timeStart)->format('d.m.Y') : '...';
        $timefEnd = !empty($timeEnd) ? date_create($timeEnd)->format('d.m.Y') : '...';
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Stundenzettel');

        $sheet->getColumnDimension('A')->setWidth(10);
        $sheet->getColumnDimension('B')->setWidth(20);
        $sheet->getColumnDimension('C')->setWidth(20);
        $sheet->getColumnDimension('D')->setWidth(16);
        $sheet->getColumnDimension('E')->setWidth(20);
        // all horizontal alignment = center
        $sheet->getStyle('A1:E' . (count($tss) + 6))->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        // fat heading
        $sheet->mergeCells('A1:E1')->setCellValue('A1', $entity->getUserDetailsNameFirst()
            . ' ' . $entity->getUserDetailsNameLast() . ' - Stundenzettel');
        $sheet->mergeCells('A2:E2')->setCellValue('A2', $timefStart . ' - ' . $timefEnd);
        $sheet->getStyle('A1:E2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->setCellValue('C3', 'Stundenlohn:')->getStyle('C3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
        $sheet->setCellValue('D3', $entity->getEquipmentPricePerHour());
        $sheet->getStyle('A1:E3')->applyFromArray($this->styleH2);

        $this->currentRow = 4;
        $sheet->setCellValue('A' . $this->currentRow, 'Nr')
            ->setCellValue('B' . $this->currentRow, 'kommen')
            ->setCellValue('C' . $this->currentRow, 'gehen')
            ->setCellValue('D' . $this->currentRow, 'Diff. Minute')
            ->setCellValue('E' . $this->currentRow, 'erstellt');
        $sheet->getStyle('A' . $this->currentRow . ':E' . $this->currentRow)->applyFromArray($this->styleStrong);

        $this->currentRow++;
        $sumRowStart = $this->currentRow;
        $sumMinutes = 0;
        foreach ($tss as $ts) {
            $sheet->setCellValue('A' . $this->currentRow, '')
                ->setCellValue('B' . $this->currentRow, date_create($ts['timesheet_time_start_f'])->format('d.m.Y H:i:s'))
                ->setCellValue('C' . $this->currentRow, date_create($ts['timesheet_time_end_f'])->format('d.m.Y H:i:s'))
                ->setCellValue('D' . $this->currentRow, $ts['timesheet_div_minute'])
                ->setCellValue('E' . $this->currentRow, $ts['user_login_create_start'] . '|' . $ts['user_login_create_end']);
            $this->currentRow++;
            $sumMinutes += $ts['timesheet_div_minute'];
        }
        $sumRowEnd = $this->currentRow - 1;
        $sheet->setCellValue('C' . $this->currentRow, 'Minuten');
        $sheet->setCellValue('D' . $this->currentRow, '=SUM(D' . $sumRowStart . ':D' . $sumRowEnd . ')');
        $sheet->getStyle('C' . $this->currentRow . ':D' . $this->currentRow)->applyFromArray($this->styleStrong);
        $this->currentRow++;
        $sheet->setCellValue('C' . $this->currentRow, 'Zeit * Std.-Lohn');
        $sheet->setCellValue('D' . $this->currentRow,
            '=ROUND((SUM(D' . $sumRowStart . ':D' . $sumRowEnd . ')/60)*D3, 2)');
        $sheet->getStyle('C' . $this->currentRow . ':D' . $this->currentRow)->applyFromArray($this->styleStrong);

        $writer = new Xlsx($spreadsheet);
        $this->fqfnFile = $this->tmpFolder . '/' . date('Y-m-d_H-i-s') . '_Stundenzettel.xlsx';
        try {
            $writer->save($this->fqfnFile);
        } catch (SpreadsheetWriterException $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
            return;
        }
        RenderTool::outputAttachment($this->fqfnFile, 'Stundenzettel.xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    }
}
