<?php

namespace Lerp\Office\Service\Order;

use Bitkorn\Trinket\Tools\Time\TimeTool;
use Lerp\Document\Service\OfferDocumentService;
use Lerp\Document\Service\OrderDocumentService;
use Lerp\Factoryorder\Service\FactoryorderService;
use Lerp\Order\Entity\Order\OrderEntity;
use Lerp\Order\Service\Order\Files\FileOrderRelService;
use Lerp\Order\Service\Order\OrderItemService;
use Lerp\Order\Service\Order\OrderService;
use Lerp\Order\Service\OrderMasterDataServiceInterface;
use Lerp\Purchase\Service\PurchaseOrder\PurchaseOrderService;
use Lerp\Office\Service\AbstractSpreadsheetService;
use Lerp\Stock\Service\StockService;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Exception as SpreadsheetWriterException;

class OrderMasterDataService extends AbstractSpreadsheetService implements OrderMasterDataServiceInterface
{
    protected int $orderNoNo;
    protected OrderService $orderService;
    protected OrderItemService $orderItemService;
    protected FileOrderRelService $fileOrderRelService;
    protected OfferDocumentService $offerDocumentService;
    protected OrderDocumentService $orderDocumentService;
    protected PurchaseOrderService $purchaseOrderService;
    protected FactoryorderService $factoryorderService;
    protected StockService $stockService;

    public function getOrderNoNo(): int
    {
        return $this->orderNoNo;
    }

    public function setOrderService(OrderService $orderService): void
    {
        $this->orderService = $orderService;
    }

    public function setOrderItemService(OrderItemService $orderItemService): void
    {
        $this->orderItemService = $orderItemService;
    }

    public function setFileOrderRelService(FileOrderRelService $fileOrderRelService): void
    {
        $this->fileOrderRelService = $fileOrderRelService;
    }

    public function setOfferDocumentService(OfferDocumentService $offerDocumentService): void
    {
        $this->offerDocumentService = $offerDocumentService;
    }

    public function setOrderDocumentService(OrderDocumentService $orderDocumentService): void
    {
        $this->orderDocumentService = $orderDocumentService;
    }

    public function setPurchaseOrderService(PurchaseOrderService $purchaseOrderService): void
    {
        $this->purchaseOrderService = $purchaseOrderService;
    }

    public function setFactoryorderService(FactoryorderService $factoryorderService): void
    {
        $this->factoryorderService = $factoryorderService;
    }

    public function setStockService(StockService $stockService): void
    {
        $this->stockService = $stockService;
    }

    /**
     * @param string $orderUuid
     * @return bool
     */
    public function createOrderMasterDataSpreadsheet(string $orderUuid): bool
    {
        $this->initSpreadsheet();
        $orderEntity = new OrderEntity();
        if (!$orderEntity->exchangeArrayFromDatabase($this->orderService->getOrder($orderUuid))) {
            $this->message = 'No order for order_uuid ' . $orderUuid;
            return false;
        }
        $this->orderNoNo = $orderEntity->getOrderNo();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Stammblatt');
        $sheet->getColumnDimension('A')->setWidth(20);
        $sheet->getColumnDimension('B')->setWidth(20);
        $sheet->getColumnDimension('C')->setWidth(20);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(20);
        $sheet->getColumnDimension('F')->setWidth(20);
        $sheet->getColumnDimension('G')->setWidth(20);
        $sheet->getColumnDimension('H')->setWidth(20);
        $sheet->getStyle('A1:E420')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        /*
         * order base data
         */
        $sheet->setCellValue('A1', 'Auftrag')->setCellValue('B1', $orderEntity->getOrderNo())
            ->getStyle('B1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $sheet->setCellValue('A2', 'Bezeichnung')->setCellValue('B2', $orderEntity->getOrderLabel());
        $sheet->setCellValue('A3', 'Kostenstelle')->setCellValue('B3', $orderEntity->getCostCentreLabel());
        $sheet->setCellValue('A4', 'Auftragsdatum')
            ->setCellValue('B4', TimeTool::isoDateToGerman($orderEntity->getOrderTimeCreate()));
        $sheet->setCellValue('A5', 'Gepl. Liefertermin')
            ->setCellValue('B5', TimeTool::isoDateToGerman($orderEntity->getOrderTimeFinishSchedule()));
        $sheet->setCellValue('A6', 'Auftr. Ersteller')->setCellValue('B6',
            $orderEntity->getUserDetailsNameFirst() . ' ' . $orderEntity->getUserDetailsNameLast()
        );
        $sheet->setCellValue('A7', 'Kunde')->setCellValue('B7', $orderEntity->getCustomerName() . ' - ' . $orderEntity->getCustomerNo());
        $sheet->setCellValue('A8', 'Erledigt')
            ->setCellValue('B8', TimeTool::isoDateToGerman($orderEntity->getOrderTimeFinishReal()) ?: 'nein');

        /*
         * order documents - offer
         */
        $docOffers = $this->offerDocumentService->getDocOffersForOrder($orderUuid);
        $sheet->setCellValue('A10', 'Angebote')->getStyle('A10')->applyFromArray($this->styleH1);
        $sheet->getRowDimension('10')->setRowHeight($this->rowHeightH1);
        $sheet->setCellValue('A11', 'Nr')
            ->setCellValue('B11', 'erstellt')
            ->setCellValue('C11', 'ersetzt')
            ->setCellValue('D11', 'Netto');
        $sheet->getStyle('A11:D11')->applyFromArray($this->styleH2);
        $this->currentRow = 12;
        foreach ($docOffers as $docOffer) {
            $sheet->setCellValue('A' . $this->currentRow, $docOffer['doc_offer_no']);
            $sheet->setCellValue('B' . $this->currentRow, TimeTool::isoDateToGerman($docOffer['doc_offer_time_create']));
            $sheet->setCellValue('C' . $this->currentRow, TimeTool::isoDateToGerman($docOffer['doc_offer_time_replace']));
            $sheet->setCellValue('D' . $this->currentRow, $docOffer['doc_offer_price']);
            $this->currentRow++;
        }

        /*
         * order documents - estimate
         */
        $this->currentRow += 2;
        $docEstimates = $this->orderDocumentService->getDocEstimatesForOrder($orderUuid);
        $sheet->setCellValue('A' . $this->currentRow, 'Kostenvor.')->getStyle('A' . $this->currentRow)->applyFromArray($this->styleH1);
        $sheet->getRowDimension($this->currentRow)->setRowHeight($this->rowHeightH1);
        $this->currentRow++;
        $sheet->setCellValue('A' . $this->currentRow, 'Nr')
            ->setCellValue('B' . $this->currentRow, 'erstellt')
            ->setCellValue('C' . $this->currentRow, 'ersetzt')
            ->setCellValue('D' . $this->currentRow, 'Netto');
        $sheet->getStyle('A' . $this->currentRow . ':D' . $this->currentRow)->applyFromArray($this->styleH2);
        $this->currentRow++;
        foreach ($docEstimates as $docdocEstimate) {
            $sheet->setCellValue('A' . $this->currentRow, $docdocEstimate['doc_estimate_no']);
            $sheet->setCellValue('B' . $this->currentRow, TimeTool::isoDateToGerman($docdocEstimate['doc_estimate_time_create']));
            $sheet->setCellValue('C' . $this->currentRow, TimeTool::isoDateToGerman($docdocEstimate['doc_estimate_time_replace']));
            $sheet->setCellValue('D' . $this->currentRow, $docdocEstimate['doc_estimate_price']);
            $this->currentRow++;
        }

        /*
         * order documents - order-confirm
         */
        $this->currentRow += 2;
        $docOrdConfirms = $this->orderDocumentService->getDocOrderConfirmsForOrder($orderUuid);
        $sheet->setCellValue('A' . $this->currentRow, 'Auftr. Best.')->getStyle('A' . $this->currentRow)->applyFromArray($this->styleH1);
        $sheet->getRowDimension($this->currentRow)->setRowHeight($this->rowHeightH1);
        $this->currentRow++;
        $sheet->setCellValue('A' . $this->currentRow, 'Nr')
            ->setCellValue('B' . $this->currentRow, 'erstellt')
            ->setCellValue('C' . $this->currentRow, 'ersetzt')
            ->setCellValue('D' . $this->currentRow, 'Netto');
        $sheet->getStyle('A' . $this->currentRow . ':D' . $this->currentRow)->applyFromArray($this->styleH2);
        $this->currentRow++;
        foreach ($docOrdConfirms as $docdocdocOrdConfirm) {
            $sheet->setCellValue('A' . $this->currentRow, $docdocdocOrdConfirm['doc_order_confirm_no']);
            $sheet->setCellValue('B' . $this->currentRow, TimeTool::isoDateToGerman($docdocdocOrdConfirm['doc_order_confirm_time_create']));
            $sheet->setCellValue('C' . $this->currentRow, TimeTool::isoDateToGerman($docdocdocOrdConfirm['doc_order_confirm_time_replace']));
            $sheet->setCellValue('D' . $this->currentRow, $docdocdocOrdConfirm['doc_order_confirm_price']);
            $this->currentRow++;
        }

        /*
         * order documents - proforma
         */
        $this->currentRow += 2;
        $docProformas = $this->orderDocumentService->getDocProformasForOrder($orderUuid);
        $sheet->setCellValue('A' . $this->currentRow, 'Proforma')->getStyle('A' . $this->currentRow)->applyFromArray($this->styleH1);
        $sheet->getRowDimension($this->currentRow)->setRowHeight($this->rowHeightH1);
        $this->currentRow++;
        $sheet->setCellValue('A' . $this->currentRow, 'Nr')
            ->setCellValue('B' . $this->currentRow, 'erstellt')
            ->setCellValue('C' . $this->currentRow, 'ersetzt')
            ->setCellValue('D' . $this->currentRow, 'Netto');
        $sheet->getStyle('A' . $this->currentRow . ':D' . $this->currentRow)->applyFromArray($this->styleH2);
        $this->currentRow++;
        foreach ($docProformas as $docProforma) {
            $sheet->setCellValue('A' . $this->currentRow, $docProforma['doc_proforma_no']);
            $sheet->setCellValue('B' . $this->currentRow, TimeTool::isoDateToGerman($docProforma['doc_proforma_time_create']));
            $sheet->setCellValue('C' . $this->currentRow, TimeTool::isoDateToGerman($docProforma['doc_proforma_time_replace']));
            $sheet->setCellValue('D' . $this->currentRow, $docProforma['doc_proforma_price']);
            $this->currentRow++;
        }

        /*
         * order documents - delivery
         */
        $this->currentRow += 2;
        $docDeliverys = $this->orderDocumentService->getDocDeliverysForOrder($orderUuid);
        $sheet->setCellValue('A' . $this->currentRow, 'Proforma')->getStyle('A' . $this->currentRow)->applyFromArray($this->styleH1);
        $sheet->getRowDimension($this->currentRow)->setRowHeight($this->rowHeightH1);
        $this->currentRow++;
        $sheet->setCellValue('A' . $this->currentRow, 'Nr')
            ->setCellValue('B' . $this->currentRow, 'erstellt')
            ->setCellValue('C' . $this->currentRow, 'ersetzt');
        $sheet->getStyle('A' . $this->currentRow . ':C' . $this->currentRow)->applyFromArray($this->styleH2);
        $this->currentRow++;
        foreach ($docDeliverys as $docDelivery) {
            $sheet->setCellValue('A' . $this->currentRow, $docDelivery['doc_delivery_no']);
            $sheet->setCellValue('B' . $this->currentRow, TimeTool::isoDateToGerman($docDelivery['doc_delivery_time_create']));
            $sheet->setCellValue('C' . $this->currentRow, TimeTool::isoDateToGerman($docDelivery['doc_delivery_time_replace']));
            $this->currentRow++;
        }

        /*
         * order documents - invoice (partial, coupon, invoice)
         */
        $this->currentRow += 2;
        $docInvoices = $this->orderDocumentService->getDocInvoicesForOrder($orderUuid);
        $sheet->setCellValue('A' . $this->currentRow, 'Anzahlung')->getStyle('A' . $this->currentRow)->applyFromArray($this->styleH1);
        $sheet->getRowDimension($this->currentRow)->setRowHeight($this->rowHeightH1);
        $this->currentRow++;
        $sheet->setCellValue('A' . $this->currentRow, 'Nr')
            ->setCellValue('B' . $this->currentRow, 'erstellt')
            ->setCellValue('C' . $this->currentRow, 'ersetzt')
            ->setCellValue('D' . $this->currentRow, 'Netto');
        $sheet->getStyle('A' . $this->currentRow . ':D' . $this->currentRow)->applyFromArray($this->styleH2);
        $this->currentRow++;
        foreach ($docInvoices as $docInvoice) {
            if ($docInvoice['doc_invoice_type'] != 'partial') {
                continue;
            }
            $sheet->setCellValue('A' . $this->currentRow, $docInvoice['doc_invoice_no']);
            $sheet->setCellValue('B' . $this->currentRow, TimeTool::isoDateToGerman($docInvoice['doc_invoice_time_create']));
            $sheet->setCellValue('C' . $this->currentRow, TimeTool::isoDateToGerman($docInvoice['doc_invoice_time_replace']));
            $sheet->setCellValue('D' . $this->currentRow, $docInvoice['doc_invoice_price']);
            $this->currentRow++;
        }
        $this->currentRow += 2;
        $sheet->setCellValue('A' . $this->currentRow, 'Gutschrift')->getStyle('A' . $this->currentRow)->applyFromArray($this->styleH1);
        $sheet->getRowDimension($this->currentRow)->setRowHeight($this->rowHeightH1);
        $this->currentRow++;
        $sheet->setCellValue('A' . $this->currentRow, 'Nr')
            ->setCellValue('B' . $this->currentRow, 'erstellt')
            ->setCellValue('C' . $this->currentRow, 'ersetzt')
            ->setCellValue('D' . $this->currentRow, 'Netto');
        $sheet->getStyle('A' . $this->currentRow . ':D' . $this->currentRow)->applyFromArray($this->styleH2);
        $this->currentRow++;
        foreach ($docInvoices as $docInvoice) {
            if ($docInvoice['doc_invoice_type'] != 'coupon') {
                continue;
            }
            $sheet->setCellValue('A' . $this->currentRow, $docInvoice['doc_invoice_no']);
            $sheet->setCellValue('B' . $this->currentRow, TimeTool::isoDateToGerman($docInvoice['doc_invoice_time_create']));
            $sheet->setCellValue('C' . $this->currentRow, TimeTool::isoDateToGerman($docInvoice['doc_invoice_time_replace']));
            $sheet->setCellValue('D' . $this->currentRow, $docInvoice['doc_invoice_price']);
            $this->currentRow++;
        }
        $this->currentRow += 2;
        $sheet->setCellValue('A' . $this->currentRow, 'Rechnung')->getStyle('A' . $this->currentRow)->applyFromArray($this->styleH1);
        $sheet->getRowDimension($this->currentRow)->setRowHeight($this->rowHeightH1);
        $this->currentRow++;
        $sheet->setCellValue('A' . $this->currentRow, 'Nr')
            ->setCellValue('B' . $this->currentRow, 'erstellt')
            ->setCellValue('C' . $this->currentRow, 'ersetzt')
            ->setCellValue('D' . $this->currentRow, 'Netto');
        $sheet->getStyle('A' . $this->currentRow . ':D' . $this->currentRow)->applyFromArray($this->styleH2);
        $this->currentRow++;
        foreach ($docInvoices as $docInvoice) {
            if ($docInvoice['doc_invoice_type'] != 'invoice') {
                continue;
            }
            $sheet->setCellValue('A' . $this->currentRow, $docInvoice['doc_invoice_no']);
            $sheet->setCellValue('B' . $this->currentRow, TimeTool::isoDateToGerman($docInvoice['doc_invoice_time_create']));
            $sheet->setCellValue('C' . $this->currentRow, TimeTool::isoDateToGerman($docInvoice['doc_invoice_time_replace']));
            $sheet->setCellValue('D' . $this->currentRow, $docInvoice['doc_invoice_price']);
            $this->currentRow++;
        }

        /*
         * purchase-order
         */
        $this->currentRow += 2;
        $purchaseOrderItems = $this->purchaseOrderService->getPurchaseOrderItemsForOrder($orderUuid);
        $sheet->setCellValue('A' . $this->currentRow, 'Bestellung')->getStyle('A' . $this->currentRow)->applyFromArray($this->styleH1);
        $sheet->getRowDimension($this->currentRow)->setRowHeight($this->rowHeightH1);
        $this->currentRow++;
        $sheet->setCellValue('A' . $this->currentRow, 'Nr')
            ->setCellValue('B' . $this->currentRow, 'Lieferant')
            ->setCellValue('C' . $this->currentRow, 'Produkt')
            ->setCellValue('D' . $this->currentRow, 'Menge')
            ->setCellValue('E' . $this->currentRow, 'ME')
            ->setCellValue('F' . $this->currentRow, 'Netto €');
        $sheet->getStyle('A' . $this->currentRow . ':F' . $this->currentRow)->applyFromArray($this->styleH2);
        $this->currentRow++;
        $sumRowStart = $this->currentRow;
        foreach ($purchaseOrderItems as $purchaseOrderItem) {
            $sheet->setCellValue('A' . $this->currentRow, $purchaseOrderItem['purchase_order_no']);
            $sheet->setCellValue('B' . $this->currentRow, $purchaseOrderItem['supplier_no'] . ' - ' . $purchaseOrderItem['supplier_name']);
            $sheet->setCellValue('C' . $this->currentRow, $purchaseOrderItem['product_no_no'] . ' - ' . $purchaseOrderItem['purchase_order_item_text_short']);
            $sheet->setCellValue('D' . $this->currentRow, $purchaseOrderItem['purchase_order_item_quantity']);
            $sheet->setCellValue('E' . $this->currentRow, $purchaseOrderItem['quantityunit_label']);
            $sheet->setCellValue('F' . $this->currentRow, $purchaseOrderItem['purchase_order_item_price_total']);
            $this->currentRow++;
        }
        $sumRowEnd = $this->currentRow - 1;
        $sheet->setCellValue('F' . $this->currentRow, '=SUM(F' . $sumRowStart . ':F' . $sumRowEnd . ')');
        $sheet->getStyle('F' . $this->currentRow)->applyFromArray($this->styleStrong);

        /*
         * order items
         */
        $this->currentRow += 2;
        $orderItems = $this->orderItemService->getOrderItemsForOrder($orderUuid);
        $sheet->setCellValue('A' . $this->currentRow, 'Autrag Items')->getStyle('A' . $this->currentRow)->applyFromArray($this->styleH1);
        $sheet->getRowDimension($this->currentRow)->setRowHeight($this->rowHeightH1);
        $this->currentRow++;
        $sheet->setCellValue('A' . $this->currentRow, 'Produkt')
            ->setCellValue('B' . $this->currentRow, 'Kostenstelle')
            ->setCellValue('C' . $this->currentRow, 'Preis €')
            ->setCellValue('D' . $this->currentRow, 'Menge')
            ->setCellValue('E' . $this->currentRow, 'ME')
            ->setCellValue('F' . $this->currentRow, 'Summe €')
            ->setCellValue('G' . $this->currentRow, 'Ums.-St. €')
            ->setCellValue('H' . $this->currentRow, 'Brutto €');
        $sheet->getStyle('A' . $this->currentRow . ':H' . $this->currentRow)->applyFromArray($this->styleH2);
        $this->currentRow++;
        $sumRowStart = $this->currentRow;
        foreach ($orderItems as $orderItem) {
            $sheet->setCellValue('A' . $this->currentRow, $orderItem['product_no_no'] . ' - ' . $orderItem['order_item_text_short']);
            $sheet->setCellValue('B' . $this->currentRow, $orderItem['cost_centre_label']);
            $sheet->setCellValue('C' . $this->currentRow, $orderItem['order_item_price']);
            $sheet->setCellValue('D' . $this->currentRow, $orderItem['order_item_quantity']);
            $sheet->setCellValue('E' . $this->currentRow, $orderItem['quantityunit_label']);
            $sheet->setCellValue('F' . $this->currentRow, $orderItem['order_item_price_total']);
            $sheet->setCellValue('G' . $this->currentRow, $orderItem['order_item_price_total_end'] - $orderItem['order_item_price_total']);
            $sheet->setCellValue('H' . $this->currentRow, $orderItem['order_item_price_total_end']);
            $this->currentRow++;
        }
        $sumRowEnd = $this->currentRow - 1;
        $sheet->setCellValue('F' . $this->currentRow, '=SUM(F' . $sumRowStart . ':F' . $sumRowEnd . ')');
        $sheet->getStyle('F' . $this->currentRow)->applyFromArray($this->styleStrong);
        $sheet->setCellValue('G' . $this->currentRow, '=SUM(G' . $sumRowStart . ':G' . $sumRowEnd . ')');
        $sheet->getStyle('G' . $this->currentRow)->applyFromArray($this->styleStrong);
        $sheet->setCellValue('H' . $this->currentRow, '=SUM(H' . $sumRowStart . ':H' . $sumRowEnd . ')');
        $sheet->getStyle('H' . $this->currentRow)->applyFromArray($this->styleStrong);

        /*
         * factoryorder
         */
        $this->currentRow += 2;
        $fos = $this->factoryorderService->getFactoryordersForOrder($orderUuid);
        $sheet->setCellValue('A' . $this->currentRow, 'Betriebsaufträge')->getStyle('A' . $this->currentRow)->applyFromArray($this->styleH1);
        $sheet->getRowDimension($this->currentRow)->setRowHeight($this->rowHeightH1);
        $this->currentRow++;
        $sheet->setCellValue('A' . $this->currentRow, 'BA Nr.')
            ->setCellValue('B' . $this->currentRow, 'Produkt')
            ->setCellValue('C' . $this->currentRow, 'Menge')
            ->setCellValue('D' . $this->currentRow, 'ME')
            ->setCellValue('E' . $this->currentRow, 'fertig Plan')
            ->setCellValue('F' . $this->currentRow, 'fertig');
        $sheet->getStyle('A' . $this->currentRow . ':F' . $this->currentRow)->applyFromArray($this->styleH2);
        $this->currentRow++;
        foreach ($fos as $fo) {
            $sheet->setCellValue('A' . $this->currentRow, $fo['factoryorder_no']);
            $sheet->setCellValue('B' . $this->currentRow, $fo['product_no_no'] . ' - ' . $fo['product_text_short']);
            $sheet->setCellValue('C' . $this->currentRow, $fo['factoryorder_quantity']);
            $sheet->setCellValue('D' . $this->currentRow, $fo['quantityunit_label']);
            $sheet->setCellValue('E' . $this->currentRow, $fo['factoryorder_time_finish_schedule']);
            $sheet->setCellValue('F' . $this->currentRow, $fo['factoryorder_time_finish_real']);
            $this->currentRow++;
        }

        /*
         * stockout
         */
        $this->currentRow += 2;
        $sos = $this->stockService->getOrderStockouts($orderUuid);
        $sheet->setCellValue('A' . $this->currentRow, 'Lagerentnahmen')->getStyle('A' . $this->currentRow)->applyFromArray($this->styleH1);
        $sheet->getRowDimension($this->currentRow)->setRowHeight($this->rowHeightH1);
        $this->currentRow++;
        $sheet->setCellValue('A' . $this->currentRow, 'Produkt')
            ->setCellValue('B' . $this->currentRow, 'Charge')
            ->setCellValue('C' . $this->currentRow, 'WE')
            ->setCellValue('D' . $this->currentRow, 'Menge')
            ->setCellValue('E' . $this->currentRow, 'ME')
            ->setCellValue('F' . $this->currentRow, 'EK €')
            ->setCellValue('G' . $this->currentRow, 'VK €');
        $sheet->getStyle('A' . $this->currentRow . ':G' . $this->currentRow)->applyFromArray($this->styleH2);
        $this->currentRow++;
        $sumRowStart = $this->currentRow;
        foreach ($sos as $so) {
            $sheet->setCellValue('A' . $this->currentRow, $so['product_no_no'] . ' - ' . $so['product_text_short']);
            $sheet->setCellValue('B' . $this->currentRow, $so['stock_charge_code']);
            $sheet->setCellValue('C' . $this->currentRow, $so['stockin_code']);
            $sheet->setCellValue('D' . $this->currentRow, $so['stockout_quantity']);
            $sheet->setCellValue('E' . $this->currentRow, $so['quantityunit_label']);
            $sheet->setCellValue('F' . $this->currentRow, $so['product_calc_price']);
            $sheet->setCellValue('G' . $this->currentRow, $so['product_calc_price_set']);
            $this->currentRow++;
        }
        $sumRowEnd = $this->currentRow - 1;
        $sheet->setCellValue('F' . $this->currentRow, '=SUM(F' . $sumRowStart . ':F' . $sumRowEnd . ')');
        $sheet->getStyle('F' . $this->currentRow)->applyFromArray($this->styleStrong);
        $sheet->setCellValue('G' . $this->currentRow, '=SUM(G' . $sumRowStart . ':G' . $sumRowEnd . ')');
        $sheet->getStyle('G' . $this->currentRow)->applyFromArray($this->styleStrong);

        /*
         * files
         */
        $this->currentRow += 2;
        $files = $this->fileOrderRelService->getFilesForOrder($orderUuid);
        $sheet->setCellValue('A' . $this->currentRow, 'Dateien')->getStyle('A' . $this->currentRow)->applyFromArray($this->styleH1);
        $sheet->getRowDimension($this->currentRow)->setRowHeight($this->rowHeightH1);
        $this->currentRow++;
        $sheet->setCellValue('A' . $this->currentRow, 'Label')
            ->setCellValue('B' . $this->currentRow, 'Beschreibung')
            ->setCellValue('C' . $this->currentRow, 'ext.')
            ->setCellValue('D' . $this->currentRow, 'Typ')
            ->setCellValue('E' . $this->currentRow, 'erstellt')
            ->setCellValue('F' . $this->currentRow, 'bearbeitet');
        $sheet->getStyle('A' . $this->currentRow . ':F' . $this->currentRow)->applyFromArray($this->styleH2);
        $this->currentRow++;
        foreach ($files as $file) {
            $sheet->setCellValue('A' . $this->currentRow, $file['file_label']);
            $sheet->setCellValue('B' . $this->currentRow, $file['file_desc']);
            $sheet->setCellValue('C' . $this->currentRow, $file['file_extension']);
            $sheet->setCellValue('D' . $this->currentRow, $file['file_mimetype']);
            $sheet->setCellValue('E' . $this->currentRow, TimeTool::isoDateToGerman($file['file_time_create']));
            $sheet->setCellValue('F' . $this->currentRow, TimeTool::isoDateToGerman($file['file_time_update']));
            $this->currentRow++;
        }

        $writer = new Xlsx($spreadsheet);
        $this->fqfnFile = $this->tmpFolder . '/' . date('Y-m-d_H-i-s') . '_AuftragStammblatt.xlsx';
        try {
            $writer->save($this->fqfnFile);
        } catch (SpreadsheetWriterException $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
            return false;
        }
        return true;
    }
}
