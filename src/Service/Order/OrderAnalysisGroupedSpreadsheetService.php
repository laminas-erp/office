<?php

namespace Lerp\Office\Service\Order;

use Lerp\Office\Service\AbstractSpreadsheetService;
use Lerp\Order\Entity\ParamsOrderAnalysis;
use Lerp\Order\Service\OrderAnalysisGroupedSpreadsheetServiceInterface;
use Lerp\Order\Table\Order\OrderAnalysisTable;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Exception as SpreadsheetWriterException;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class OrderAnalysisGroupedSpreadsheetService extends AbstractSpreadsheetService implements OrderAnalysisGroupedSpreadsheetServiceInterface
{
    protected OrderAnalysisTable $orderAnalysisTable;

    public function setOrderAnalysisTable(OrderAnalysisTable $orderAnalysisTable): void
    {
        $this->orderAnalysisTable = $orderAnalysisTable;
    }

    public function createOrderAnalysisGroupedSpreadsheet(ParamsOrderAnalysis $paramsOrderAnalysis): bool
    {
        if (empty($oas = $this->orderAnalysisTable->getOrderAnalysisGrouped($paramsOrderAnalysis))) {
            return false;
        }
        $group = $paramsOrderAnalysis->getGroupBy();
        $this->initSpreadsheet();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Auftrag Analyse');
        $sheet->getColumnDimension('A')->setWidth(30);
        $sheet->getColumnDimension('B')->setWidth(20);
        $sheet->getColumnDimension('C')->setWidth(20);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(20);
        $sheet->getColumnDimension('F')->setWidth(20);

        $sheet->setCellValue('A1', 'Auftrag Analyse');
        switch ($group) {
            case OrderAnalysisTable::GROUP_MONTH:
                $sheet->setCellValue('A2', 'Monat');
                break;
            case OrderAnalysisTable::GROUP_COST_CENTRE:
                $sheet->setCellValue('A2', 'Kostenstelle');
                break;
        }
        $sheet->setCellValue('B2', 'Anzahl');
        $sheet->setCellValue('C2', 'Netto');
        $sheet->setCellValue('D2', 'Netto kumm.');
        $sheet->setCellValue('E2', 'Anzahl offen');
        $sheet->setCellValue('F2', 'Netto offen');
        $sheet->getStyle('A1')->applyFromArray($this->styleStrong);
        $sheet->getStyle('A2:F2')->applyFromArray($this->styleStrong);

        $row = $row1 = 3;
        foreach ($oas as $oa) {
            switch ($group) {
                case OrderAnalysisTable::GROUP_MONTH:
                    $sheet->setCellValue("A$row", $this->monthLabel($oa['order_time_create_month']));
                    break;
                case OrderAnalysisTable::GROUP_COST_CENTRE:
                    $sheet->setCellValue("A$row", $oa['cost_centre_label']);
                    break;
            }
            $sheet->setCellValue("B$row", $oa['count_order_analysis']);
            $sheet->setCellValue("C$row", $oa['sum_order_item_price']);
            $sheet->setCellValue("D$row", $oa['sum_order_item_price_cumul']);
            $sheet->setCellValue("E$row", $oa['count_order_analysis'] - $oa['count_finish_real']);
            $sheet->setCellValue("F$row", $oa['sum_order_item_price'] - $oa['sum_order_item_price_finish']);
            $row++;
        }
        $row2 = $row - 1;
        $sheet->setCellValue("A$row", 'Summen');
        $sheet->setCellValue("B$row", "=SUM(B$row1:B$row2)");
        $sheet->setCellValue("C$row", "=SUM(C$row1:C$row2)");
        $sheet->setCellValue("D$row", "=SUM(D$row1:D$row2)");
        $sheet->setCellValue("E$row", "=SUM(E$row1:E$row2)");
        $sheet->setCellValue("F$row", "=SUM(F$row1:F$row2)");
        $sheet->getStyle("A$row:F$row")->applyFromArray($this->styleStrongGreen);
        $row++;
        $sheet->setCellValue("A$row", 'Durchschnitt');
        $sheet->setCellValue("B$row", "=AVERAGE(B$row1:B$row2)");
        $sheet->setCellValue("C$row", "=AVERAGE(C$row1:C$row2)");
        $sheet->setCellValue("D$row", "=AVERAGE(D$row1:D$row2)");
        $sheet->setCellValue("E$row", "=AVERAGE(E$row1:E$row2)");
        $sheet->setCellValue("F$row", "=AVERAGE(F$row1:F$row2)");
        $sheet->getStyle("A$row:F$row")->applyFromArray($this->styleStrongYello);

        $writer = new Xlsx($spreadsheet);
        $this->fqfnFile = $this->tmpFolder . '/' . date('Y-m-d_H-i-s') . '_AuftragAnalyseGruppiert.xlsx';
        try {
            $writer->save($this->fqfnFile);
        } catch (SpreadsheetWriterException $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
            return false;
        }
        return true;
    }

    protected function monthLabel(string $month): string
    {
        $monthLabel = date_create_from_format('Y-m-d H:i:s', $month);
        if (false === $monthLabel) {
            return '';
        }
        return $monthLabel->format('F');
    }
}
