<?php

namespace Lerp\Office\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class AbstractSpreadsheetService extends AbstractService
{
    protected int $currentRow = 0;
    protected string $tmpFolder;
    protected string $fqfnFile;
    protected NumberFormatService $numberFormatService;

    public function setNumberFormatService(NumberFormatService $numberFormatService): void
    {
        $this->numberFormatService = $numberFormatService;
    }

    protected array $styleText = [
        'font'      => [
            'bold' => false,
            'size' => 10,
        ],
        'alignment' => [
            'horizontal' => Alignment::HORIZONTAL_LEFT
        ]
    ];

    protected array $styleStrong = [
        'fill' => [
            'fillType'   => Fill::FILL_SOLID,
            'startColor' => ['rgb' => 'cccccc']
        ],
        'font' => [
            'bold' => false,
            'size' => 12,
        ],
    ];

    protected array $styleStrongGreen = [
        'fill' => [
            'fillType'   => Fill::FILL_SOLID,
            'startColor' => ['rgb' => '009933']
        ],
        'font' => [
            'bold' => false,
            'size' => 12,
        ],
    ];

    protected array $styleStrongYello = [
        'fill' => [
            'fillType'   => Fill::FILL_SOLID,
            'startColor' => ['rgb' => 'ffcc00']
        ],
        'font' => [
            'bold' => false,
            'size' => 12,
        ],
    ];

    protected array $styleStrongOrange = [
        'fill' => [
            'fillType'   => Fill::FILL_SOLID,
            'startColor' => ['rgb' => 'ff6600']
        ],
        'font' => [
            'bold' => false,
            'size' => 12,
        ],
    ];

    protected array $styleH1 = [
        'font' => [
            'bold' => false,
            'size' => 16,
        ],
    ];
    protected float $rowHeightH1 = 18;

    protected array $styleH2 = [
        'fill' => [
            'fillType'   => Fill::FILL_SOLID,
            'startColor' => ['rgb' => 'ff4814']
        ],
        'font' => [
            'bold' => false,
            'size' => 14,
        ],
        //['alignment' => ['horizontal' => Alignment::HORIZONTAL_CENTER]]
    ];
    protected float $rowHeightH2 = 16;

    public function setTmpFolder(string $tmpFolder): void
    {
        $this->tmpFolder = $tmpFolder;
    }

    public function getFqfnFile(): string
    {
        return $this->fqfnFile;
    }

    protected function initSpreadsheet(): void
    {
        if (!file_exists($this->tmpFolder) || !is_writable($this->tmpFolder)) {
            throw new \RuntimeException('Folder ' . $this->tmpFolder . ' does not exist or is not writeable - in class ' . __CLASS__);
        }

    }
}
